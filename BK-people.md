# Besut Kode people

## Post University - Mentors

Bandung/Jakarta

* [Hendy Irawan](https://www.linkedin.com/in/hendyirawan/) - [ceefour](https://github.com/ceefour) - Java, GIS, data science, front-end and back-end

Bali

* [Dmytro Sadovnychyi](https://www.linkedin.com/in/sadovnychyi/) - [sadovnychyi](http://git-awards.com/users/sadovnychyi) - Python, machine learning

Singapore

* [Tasya Aditya Rukmana](https://www.linkedin.com/in/tasya-aditya-rukmana-9ba670112) - [tadityar](https://github.com/tadityar) - PHP, Python, Java, DevOps

## University

Bandung, Telkom Universitas Programming Club:

* [Yana Siswanto](https://www.linkedin.com/in/yanaagun/) [bekicot](https://www.openhub.net/accounts/bekicot)   - Ruby, JavaScript, front-end website, data processing
* [Wisnu Adi Nurcahyo](https://www.linkedin.com/in/wisn98/) - [wisn](https://www.openhub.net/accounts/wisn) - Haskell, JavaScript, front-end website, data processing
* [Satria H R Harsono](https://www.linkedin.com/in/satriahrharsono/) - [hafizhme](https://github.com/hafizhme) - JavaScript, front-end website
* [Setyo Nugroho](https://www.linkedin.com/in/setyongr/) - [setyongr](https://github.com/setyongr) - JavaScript, front-end website
* [Ki Ageng Satria Pamungkas](https://www.linkedin.com/in/ki-ageng-satria-pamungkas-117494135/) - [pamungkaski](https://github.com/pamungkaski) - Python, front-end
* [Ginanjar Widya](https://www.linkedin.com/in/ginanjarwidya/) - [ginaaaan](https://github.com/ginaaaan) - Java, Android apps
* [Afnizar Nur Ghifari](https://www.linkedin.com/in/afnizarnur/) - [afnizarnur](https://github.com/afnizarnur) - JavaScript, CoffeeScript, design

Jakarta

* [Andika Demas Riyandi](https://www.linkedin.com/in/andika-riyandi-b1b41336/) - [Rizary](https://github.com/Rizary) - Haskell
* [Zulfikar Ijul](https://www.facebook.com/zulfikar97) - [Z-Fikar](https://github.com/Z-Fikar) - GIS, SPARQL
* [Livia Andriana Lohanda](https://www.facebook.com/livia.andriana) - [chocochino](https://github.com/chocochino) - Internationalisation and Localisation

Yogyakarta:

* [Adhika Setya Pramudita](https://www.linkedin.com/in/adhikasp/) - [adhikasp](https://github.com/adhikasp) - Python, code quality, IoT

Cilegon:

* [Iqbal Mohammad Abdul Ghoni](https://www.linkedin.com/in/arsfiqball/) - [Arsfiqball](https://github.com/Arsfiqball) - JavaScript, front-end, IoT

Cilacap/Semarang:

* [Prasasto Adi](https://www.linkedin.com/in/prasastoadi) - [prasastoadi](https://github.com/prasastoadi) - Python, machine learning

Surakarta:

* [Didik Setiawan](https://www.linkedin.com/in/didiksetiawan/) - [dstw](https://github.com/dstw) - C, secure protocols, system operations

Bogor:

* [Syah Dwi Prihatmoko](https://www.linkedin.com/in/sdmoko/) - [sdmoko](https://github.com/sdmoko) - DevOps, openSUSE

Surabaya:

* [Saniyaayu Sekarpramesi](https://www.linkedin.com/in/ssekarpramesi/) - [sekarpramesi](https://github.com/sekarpramesi) - PHP, MySQL

## High school

Jakarta

* [Kaisar Arkhan](https://www.linkedin.com/in/yukiisbored/) - [yuki_is_bored](https://www.openhub.net/accounts/yuki_is_bored) - Python, DevOps
* [Scott Moses Sunarto](https://www.linkedin.com/in/smsunarto/) - [smsunarto](https://github.com/smsunarto) - Java, design

Bandung

* [Raefaldhi Amartya](https://www.linkedin.com/in/raefaldhi-amartya-junior-31220081/) - [raefaldhia](https://github.com/raefaldhia) - C, code quality

Kebumen

* [Rafid Aslam](https://www.linkedin.com/in/rafid-a-ab2199135/) - [refeed](https://www.openhub.net/accounts/refeed)  - Python, bots, communication, data mining
